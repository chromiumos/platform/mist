// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "mist/mock_udev_enumerate.h"

namespace mist {

MockUdevEnumerate::MockUdevEnumerate() {}

MockUdevEnumerate::~MockUdevEnumerate() {}

}  // namespace mist
