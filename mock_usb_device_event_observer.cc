// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "mist/mock_usb_device_event_observer.h"

namespace mist {

MockUsbDeviceEventObserver::MockUsbDeviceEventObserver() {}

MockUsbDeviceEventObserver::~MockUsbDeviceEventObserver() {}

}  // namespace mist
